"use strict";

const axios = require('axios').default;

module.exports.authenticate = async (request) => {

    const body = JSON.parse(request?.body||"{}");
    let response = false;

    if (body?.login && body?.pass) {
        response = (await axios.get('https://official-joke-api.appspot.com/random_joke'))?.data;
    }

    return {
        statusCode: 200,
        body: !response ? `Invalid login "${body?.login}"!` : {
            login: body?.login||'unknown',
            question: response?.setup,
            answer: response?.punchline
        }
    };
};
